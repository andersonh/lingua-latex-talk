# *LaTeX: An Interactive Introduction,* Additional Resources

## LaTeX distributions and Editors
To edit LaTeX, you need to use either an online editing service, or download and install a LaTeX distribution and editor to your computer.

### Online editors
Using an online service is the easiest--you don't have to install anything, and like Google Docs, you can access it from any computer.  I currently recommend [Overleaf](https://www.overleaf.com); accounts are free, and you can have up to one collaborator at a time.  It also has a nice auto-compile feature, so your doucment gets updated as you write it.  (You can have more than one other person editing the doucment if you buy a subscription)

### Installing to your computer

LaTeX distributions are, for all intents and purposes, interchangeable.  The two big ones are [MikTex](https://miktex.org/) and [TexLive](https://tug.org/texlive/); just pick one, download it, and you'll be fine.  I highly recommend selecting the option to install *all* packages, just to make sure you have absolutely everything you could ever need.  Note that this will take up a few gigabytes on your hard drive, but it will save you a bunch of hassle with manually installing packages later.

There are dozens of good editors out there.  I personally like and use [TexStudio](https://www.texstudio.org/), but if you look around a bit you might find something else that's more to your tastes.

If you're on most Linux distributions, you can install all of this through your package manager.

# Resources
- The [LaTeX wikibook](https://en.wikibooks.org/wiki/LaTeX) is one of the best single resources out there.  Great for learning from scratch and for learning to do more sophisticated things.  I constantly find myself referring back to this.
- [Luke Smith has an excellent, quick series of tutorials on using LaTeX on his YouTube channel.](https://www.youtube.com/playlist?list=PL-p5XmQHB_JSQvW8_mhBdcwEyxdVX0c1T)